function GoogleDrive() {}

GoogleDrive.prototype.disconnect = function (successCallback, errorCallback) {
    cordova.exec(successCallback, errorCallback, "GoogleDrive", "disconnect", []);
};

GoogleDrive.prototype.createFile = function (fileName, dataString, successCallback, errorCallback) {
    cordova.exec(successCallback, errorCallback, "GoogleDrive", "createFile", [fileName, dataString]);
};

GoogleDrive.prototype.searchFile = function (fileName, successCallback, errorCallback) {
    cordova.exec(successCallback, errorCallback, "GoogleDrive", "searchFile", [fileName]);
};

GoogleDrive.prototype.readFile = function (fileID, successCallback, errorCallback) {
    cordova.exec(successCallback, errorCallback, "GoogleDrive", "readFile", [fileID]);
};

GoogleDrive.prototype.modifyFile = function (fileID, dataString, successCallback, errorCallback) {
    cordova.exec(successCallback, errorCallback, "GoogleDrive", "modifyFile", [fileID, dataString]);
};

GoogleDrive.prototype.getEmail = function (successCallback, errorCallback) {
    cordova.exec(successCallback, errorCallback, "GoogleDrive", "getEmail", []);
};

GoogleDrive.install = function () {
    if (!window.plugins) {
        window.plugins = {};
    }

    window.plugins.gdrive = new GoogleDrive();
    return window.plugins.gdrive;
};

cordova.addConstructor(GoogleDrive.install);
