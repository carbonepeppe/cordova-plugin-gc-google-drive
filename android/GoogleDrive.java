package gr.gc;

import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONException;

import android.content.Context;

import android.content.Intent;
import android.content.IntentSender;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.content.SharedPreferences;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveApi;
import com.google.android.gms.drive.DriveContents;
import com.google.android.gms.drive.DriveFile;
import com.google.android.gms.drive.DriveFolder;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.DriveResource;
import com.google.android.gms.drive.Metadata;
import com.google.android.gms.drive.MetadataBuffer;
import com.google.android.gms.drive.MetadataChangeSet;
import com.google.android.gms.drive.query.Filters;
import com.google.android.gms.drive.query.Query;
import com.google.android.gms.drive.query.SearchableField;
import com.google.android.gms.common.AccountPicker;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import android.os.ParcelFileDescriptor;
import java.io.FileInputStream;
import android.accounts.AccountManager;

import static android.app.Activity.RESULT_OK;

public class GoogleDrive extends CordovaPlugin implements GoogleApiClient.ConnectionCallbacks,
GoogleApiClient.OnConnectionFailedListener {

	private static final String TAG = "GoogleDrivePlugin";
	private static final int REQUEST_CODE_RESOLUTION = 3;
	private static final int SOME_REQUEST_CODE = 8;
	private static final String MY_PREFS_NAME = "MyPrefsFile";
	private GoogleApiClient mGoogleApiClient;
	private String mAction = "";
	private String toLocalDest;
	private String fileid;
	private String localFPath;
	private boolean appFolder;
	private String nameFile;
	private String dataString;
	private String accountName;
	private String account;
	private CallbackContext mCallbackContext;

	@Override
	public void initialize(CordovaInterface cordova, CordovaWebView webView) {
		super.initialize(cordova, webView);
		if (mGoogleApiClient == null) {
			mGoogleApiClient = new GoogleApiClient.Builder(cordova.getActivity()).addApi(Drive.API).addScope(Drive.SCOPE_FILE).addConnectionCallbacks(this).addOnConnectionFailedListener(this).build();
		}
		Log.i(TAG, "Plugin initialized");
	}

	@Override
	public boolean execute(String action, final JSONArray args, final CallbackContext callbackContext) throws JSONException {
		//JSONObject jobject = args.getJSONObject(0);
		mCallbackContext = callbackContext;
		mAction = action;
		if ("createFile".equals(action)) {
			//  callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR,"mio_test"));
			cordova.getThreadPool().execute(new Runnable() {@Override
				public void run() {
					try {
						nameFile = args.getString(0);
						dataString = args.getString(1);

						setAccount();

						if (mGoogleApiClient.isConnected()) {

							createFile(nameFile, dataString);

						} else mGoogleApiClient.connect();
					} catch(JSONException ex) {
						callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, "mio_test"));
					}
				}
			});
			return true;
		} else if ("getEmail".equals(action)) {
			cordova.getThreadPool().execute(new Runnable() {@Override
				public void run() {
					try {

						setAccount();

						if (mGoogleApiClient.isConnected()) {

							getEmail();

						} else mGoogleApiClient.connect();
					} catch(Exception ex) {
						callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, "mio_test"));
					}
				}
			});
			return true;
		} else if ("searchFile".equals(action)) {
			cordova.getThreadPool().execute(new Runnable() {@Override
				public void run() {
					try {
						nameFile = args.getString(0);

						setAccount();

						if (mGoogleApiClient.isConnected()) {

							searchFile(nameFile);

						} else mGoogleApiClient.connect();
					} catch(JSONException ex) {
						callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, "mio_test"));
					}
				}
			});
			return true;
		} else if ("readFile".equals(action)) {
			cordova.getThreadPool().execute(new Runnable() {@Override
				public void run() {
					try {
						fileid = args.getString(0);

						setAccount();

						if (mGoogleApiClient.isConnected()) {

							readFile(fileid);

						} else mGoogleApiClient.connect();
					} catch(JSONException ex) {
						callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, "mio_test"));
					}
				}
			});
			return true;
		} else if ("modifyFile".equals(action)) {
			cordova.getThreadPool().execute(new Runnable() {@Override
				public void run() {
					try {
						fileid = args.getString(0);
						dataString = args.getString(1);

						setAccount();

						if (mGoogleApiClient.isConnected()) {

							modifyFile(fileid, dataString);

						} else mGoogleApiClient.connect();
					} catch(JSONException ex) {
						callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, "mio_test"));
					}
				}
			});
			return true;
		} else if ("disconnect".equals(action)) {
			mGoogleApiClient.disconnect();
			callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK));
			return true;
		}
		return false;
	}

	private static String getStringFromInputStream(InputStream is) {

		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();

		String line;
		try {

			br = new BufferedReader(new InputStreamReader(is));
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}

		} catch(IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch(IOException e) {
					e.printStackTrace();
				}
			}
		}

		return sb.toString();

	}

	private void getEmail() {
		try {

			mCallbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, accountName));

		} catch(Exception ex) {
			Log.i(TAG, ex.getMessage());
		}
	}

	private void readFile(final String fileid) {
		DriveFile.DownloadProgressListener listener = new DriveFile.DownloadProgressListener() {@Override
			public void onProgress(long bytesDownloaded, long bytesExpected) {
				int progress = (int)(bytesDownloaded * 100 / bytesExpected);
				Log.d(TAG, String.format("Loading progress: %d percent", progress));
				//mProgressBar.setProgress(progress);
			}
		};
		final DriveFile file = DriveId.decodeFromString(fileid).asDriveFile();
		file.open(mGoogleApiClient, DriveFile.MODE_READ_ONLY, listener).setResultCallback(new ResultCallback < DriveApi.DriveContentsResult > () {@Override
			public void onResult(@NonNull final DriveApi.DriveContentsResult result) {
				final DriveContents driveContents = result.getDriveContents();

				if (!result.getStatus().isSuccess()) {
					mCallbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, "Something went wrong with file download"));
					return;
				}
				new Thread() {@Override
					public void run() {
						try {
							InputStream inputStream = driveContents.getInputStream();

							String result = getStringFromInputStream(inputStream);

							inputStream.close();

							try {
								//   mCallbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, new JSONObject().put("fileId",file.getDriveId())));
								mCallbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, new JSONObject().put("dataString", result)));
							} catch(JSONException ex) {
								Log.i(TAG, ex.getMessage());
							}
						} catch(IOException e) {
							Log.e(TAG, e.getMessage());
						}
					}
				}.start();
			}
		});
	}

	private void modifyFile(final String fileid, final String dataString) {
		DriveFile.DownloadProgressListener listener = new DriveFile.DownloadProgressListener() {@Override
			public void onProgress(long bytesDownloaded, long bytesExpected) {
				int progress = (int)(bytesDownloaded * 100 / bytesExpected);
				Log.d(TAG, String.format("Loading progress: %d percent", progress));
				//mProgressBar.setProgress(progress);
			}
		};
		final DriveFile file = DriveId.decodeFromString(fileid).asDriveFile();
		file.open(mGoogleApiClient, DriveFile.MODE_WRITE_ONLY, null).setResultCallback(new ResultCallback < DriveApi.DriveContentsResult > () {@Override
			public void onResult(@NonNull final DriveApi.DriveContentsResult result) {
				final DriveContents contents = result.getDriveContents();

				if (!result.getStatus().isSuccess()) {
					mCallbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, "Something went wrong with file download"));
					return;
				}
				new Thread() {@Override
					public void run() {
						try {

							OutputStream outputStream = contents.getOutputStream();

							Writer writer = new OutputStreamWriter(outputStream);

							writer.write(dataString);
							writer.close();

							contents.commit(mGoogleApiClient, null).setResultCallback(new ResultCallback < Status > () {@Override
								public void onResult(Status status) {
									try {
										if (status.isSuccess()) {

											mCallbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, new JSONObject().put("fileId", file.getDriveId())));

										}
										else {

											mCallbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, "Something went wrong with modify"));

										}
									} catch(JSONException ex) {
										Log.i(TAG, ex.getMessage());
									}
								}
							});

						} catch(IOException e) {
							Log.e(TAG, e.getMessage());
						}
					}
				}.start();
			}
		});
	}

	private void createFile(final String nameFile, final String dataString) {
		Drive.DriveApi.newDriveContents(mGoogleApiClient).setResultCallback(new ResultCallback < DriveApi.DriveContentsResult > () {

			@Override
			public void onResult(DriveApi.DriveContentsResult result) {
				final DriveContents driveContents = result.getDriveContents();

				if (!result.getStatus().isSuccess()) {
					mCallbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, "Failed to create new contents"));
					return;
				}

				new Thread() {@Override
					public void run() {
						Log.i(TAG, "New contents created.");
						OutputStream outputStream = driveContents.getOutputStream();

						Writer writer = new OutputStreamWriter(outputStream);
						try {
							writer.write(dataString);
							writer.close();
						} catch(IOException e) {
							Log.e(TAG, e.getMessage());
						}

						//Log.i(TAG,fname);
						MetadataChangeSet metadataChangeSet = new MetadataChangeSet.Builder().setMimeType("application/octet-stream").setTitle(nameFile).build();
						DriveFolder uploadFolder = Drive.DriveApi.getRootFolder(mGoogleApiClient);

						uploadFolder.createFile(mGoogleApiClient, metadataChangeSet, driveContents).setResultCallback(new ResultCallback < DriveFolder.DriveFileResult > () {@Override
							public void onResult(DriveFolder.DriveFileResult result) {
								if (result.getStatus().isSuccess()) {
									try {
										mCallbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, new JSONObject().put("fileId", result.getDriveFile().getDriveId())));
									} catch(JSONException ex) {
										Log.i(TAG, ex.getMessage());
									}
									Log.i(TAG, result.getDriveFile().getDriveId() + "");
								}
							}
						});
					}
				}.start();
			}
		});
	}

	private void searchFile(final String fleName) {

		Query.Builder qb = new Query.Builder();

		qb.addFilter(Filters.and(
		Filters.eq(SearchableField.TITLE, fleName), Filters.eq(SearchableField.TRASHED, false)));

		Query query = qb.build();

		Drive.DriveApi.query(mGoogleApiClient, query).setResultCallback(new ResultCallback < DriveApi.MetadataBufferResult > () {@Override
			public void onResult(DriveApi.MetadataBufferResult result) {
				if (!result.getStatus().isSuccess()) {
					mCallbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, "failed to retrieve file list"));
					return;
				}
				MetadataBuffer flist = result.getMetadataBuffer();
				JSONArray response = new JSONArray();
				for (Metadata file: flist) {
					try {
						response.put(new JSONObject().put("name", file.getTitle()).put("created", file.getCreatedDate().toString()).put("id", file.getDriveId()));
					} catch(JSONException ex) {}
				}
				JSONObject flistJSON = new JSONObject();
				try {
					flistJSON.put("flist", response);
				} catch(JSONException ex) {}
				mCallbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, flistJSON));
				flist.release();
				//Log.i(TAG,flist.toString());
			}
		});
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		/*
        if ((requestCode == REQUEST_CODE_RESOLUTION && resultCode == RESULT_OK) && (data.getExtras() != null )) {

            mGoogleApiClient.connect();
        }
     */

		if (requestCode == SOME_REQUEST_CODE && resultCode == RESULT_OK) {
			accountName = data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
			SharedPreferences.Editor editor = (cordova.getActivity()).getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE).edit();
			editor.putString("accountName", accountName);
			editor.commit();

			Log.i(TAG, "accountName: " + accountName);

			mGoogleApiClient = null;
			if (mGoogleApiClient == null) {
				mGoogleApiClient = new GoogleApiClient.Builder(cordova.getActivity()).addApi(Drive.API).addScope(Drive.SCOPE_FILE).setAccountName(accountName).addConnectionCallbacks(this).addOnConnectionFailedListener(this).build();
			}

			mGoogleApiClient.connect();
		}

	}

	@Override
	public void onConnectionFailed(ConnectionResult result) {
		// Called whenever the API client fails to connect.
		Log.i(TAG, "GoogleApiClient connection failed: " + result.toString());
		/*
        if (!result.hasResolution()) {
            // show the localized error dialog.
            GoogleApiAvailability.getInstance().getErrorDialog(cordova.getActivity(), result.getErrorCode(), 0).show();
            return;
        }
     */

		try {
			Log.i(TAG, "trying to resolve issue...");
			cordova.setActivityResultCallback(this);

			if (accountName != null) {
				mGoogleApiClient = null;
				if (mGoogleApiClient == null) {
					mGoogleApiClient = new GoogleApiClient.Builder(cordova.getActivity()).addApi(Drive.API).addScope(Drive.SCOPE_FILE).setAccountName(accountName).addConnectionCallbacks(this).addOnConnectionFailedListener(this).build();
				}

				mGoogleApiClient.connect();

			} else {
				Intent intent = AccountPicker.newChooseAccountIntent(null, null, new String[] {
					"com.google"
				},
				false, null, null, null, null);
				(cordova.getActivity()).startActivityForResult(intent, SOME_REQUEST_CODE);
			}

		} catch(Exception e) {
			Log.e(TAG, "Exception while starting resolution activity", e);
		}
	}

	@Override
	public void onConnected(Bundle connectionHint) {
		Log.i(TAG, "API client connected.");
		if (mAction.equals("createFile")) {
			createFile(nameFile, dataString);
		} else if (mAction.equals("searchFile")) {
			searchFile(nameFile);
		} else if (mAction.equals("readFile")) {
			readFile(fileid);
		} else if (mAction.equals("modifyFile")) {
			modifyFile(fileid, dataString);
		} else if (mAction.equals("getEmail")) {
			getEmail();
		}
	}

	@Override
	public void onConnectionSuspended(int cause) {
		Log.i(TAG, "GoogleApiClient connection suspended");
	}

	private void setAccount() {
		if (account == null) {
			SharedPreferences prefs = (cordova.getActivity()).getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE);
			accountName = prefs.getString("accountName", null);
			Log.i(TAG, "accountName: " + accountName);
		}
	}
}
